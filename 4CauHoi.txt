﻿1. Lợi ích mà website mang lại:
   - Trong xã hội ngày nay ăn chay đang dần là một khuynh hướng đang thịnh hành. Có nhiều lý do để ăn chay như: Vì niềm tin tôn giáo, 
đạo đức (bảo vệ động vật), tư tưởng triết học, bảo vệ và giúp cân bằng môi trường sinh thái, kinh tế, ăn chay thường rẻ hơn ăn mặn, vì 
do sức khỏe cần kiêng thịt... . Tuy nhiên nếu quyết định ăn chay, nhất là ăn chay tuyệt đối và lâu dài chúng ta cần phải lưu ý đến
nhiều vấn đề để đảm bảo được sức khỏe
   - Website ra đời nhầm giúp người dùng hiểu biết đúng hơn về ăn chay, biết cách phối hợp các loại thực phẩm chay tạo ra những món ăn
thơm ngon cho gia đình và người thân, có được thực đơn chay lâu dài đảm bảo được sức khỏe và có thể phòng chống bệnh tật, hiểu hơn 
những lợi ích mà ăn chay mang lại cho sức khỏe và môi trường, ăn chay là bảo vệ môi trường, bảo vệ sự sống của chúng ta. Ngoài ra
website cũng là nơi giới thiệu các quán ăn, nhà hàng chay ... uy tín, để chúng ta có thể đến và thưởng thức. Website có chuyên mục Sức
khỏe là nơi giải quyêt những thức mắc, khó khăn mà người dùng vấp phải khi ăn chay.
   - Website là nơi để người dùng chia sẽ những kinh nghiệm nấu ăn, những quán ăn nhà hàng thơm ngon bổ rẻ ... cho mọi người cùng biết.
   - Ngoài ra nơi đây cũng sẽ là một trang thông tin uy tín để các nhà hàng chay quảng cáo thương hiệu, tuyển các đầu bếp chuyên nghiệp,
và các trường nấu ăn cũng có thể đăng tin tuyển học viên trên website...
2. Sự khác biệt so với website cùng loại:
   - Hiện tại trên thị trường chưa có website nào cùng loại, chỉ có những website giới thiệu về nấu ăn (nhưng đa phần cũng là món ăn mặn), hoặc
giới thiệu về nhà hàng quán ăn chay cũng rất sơ xài, chưa có hướng đến người dùng đúng cách.
3. Nếu không dùng website người dùng có thể bỏ qua những lợi ích: website là nơi trao đổi kinh nghiệm, sau những ngày làm việc, học tập mệt
mõi rồi chúng ta lên website tìm những công thức chế biến mới (cũng chính từ các thành viên của website chia sẽ) rối sẽ cùng với nấu với bạn bè
con cái, vừa để xả tress vừa có những món ăn thơm ngon, ngoài ra hàng tháng website còn có những quà tặng hấp dành cho các thành viên tích cực
của website, bạn thắc mắc về sức khỏe khi ăn chay bạn không biết hỏi ai, thì nơi đây sẽ giải quyết cho bạn tất cả điều đó.
4. Website sẽ là PainKiller cho tất cả người dùng là những người đang ăn chay trường, và Vitamin cho người dùng ăn chay định kỳ.
﻿MSSV: 12110013
Trong xã hội ngày nay Anh văn trở thành một ngôn ngữ tất yếu không thể thiếu với tất mọi người. Đặt biệt là các bạn sinh viên muốn có thể 
ra được trường thì phải có những tấm bằng Toeic, Ielts, Toefl … như trường Đại học Sư phạm kỹ thuật Tp.HCM, nếu bạn muốn ra được trường 
bạn phải đạt được chuẩn đầu ra đầu tiên là Toeic 500. Rất nhiều bạn đã ra trường nhưng không tìm được những việc làm ưng ý vì trình độ Anh 
văn còn quá kém. Nhận thấy số lượng người có nhu cầu học Anh văn là rất lớn, nhưng không phải ai cũng đủ điều kiện đến những trung tâm Anh 
ngữ uy tín, họ không có thời gian đi lại, họ không có cách nào tiếp cận được với Anh văn thường xuyên.
Vì vậy, khi được mời yêu cầu đầu tư vào website học Anh văn online của bạn tôi thấy rất hào hứng. Vì nó sẽ có một số lượng người dùng lớn. 
Khi được nghe về những tiện ích mà website của bạn mang đến cho người dùng, và cũng như sự thông minh của nó có thể đưa ra những tài liệu 
phù hợp cho những người cần đến nó tôi lại càng tin tưởng hơn về nó. Với số lượng người dùng lớn, và tính năng đặt biệt của website như vậy, 
nó sẽ dần được tất cả mọi người biết đên, khi nhắc đến học Anh văn online thì website của bạn sẽ là sự lựa chọn đầu tiên của tất cả mọi người. 
Như thế tôi tin rằng website của bạn sẽ có được những khoảng doanh thu khổng lồ từ việc đăng quảng cáo của các sản phẩm, quảng cáo các trung 
tâm Anh ngữ uy tính, và từ việc bán đĩa CD/DVD cho người dùng. Từ đó tôi đã quyết định đầu tư vào dự án website đó của bạn, hy vọng chúng 
có thể có được nhiều lợi nhuận hơn từ nó.

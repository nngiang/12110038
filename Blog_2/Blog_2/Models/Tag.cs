﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Tag
    {
        public int TagID { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [StringLength(100, ErrorMessage = "Số lượng ký tự trong khoản 10 đến 100 từ", MinimumLength = 10)]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}
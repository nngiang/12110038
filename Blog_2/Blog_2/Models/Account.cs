﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Account
    {
        public int AccountID { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [StringLength(100, ErrorMessage = "Nhập tối đa 100 ký tự")]
        public String FirstName { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [StringLength(100, ErrorMessage = "Nhập tối đa 100 ký tự")]
        public String LastName { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [DataType(DataType.Password)]
        public String Password { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Email vừa nhập không đúng. Vui lòng nhập lại")]
        public String Email { set; get; }
        
        public virtual ICollection<Post> Posts { set; get; }
    }
}
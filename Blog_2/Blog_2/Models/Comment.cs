﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Comment
    {
        public int CommentID { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        public String Author { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Post
    {
        public int PostID { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự trong khoản 20 đến 500 từ", MinimumLength = 20)]
        public String Title { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }

        [Required(ErrorMessage = "Field này phải được nhập nội dung")]
        public int AccountID { set; get; }
        public virtual Account Accounts { set; get; }
    
    }
}
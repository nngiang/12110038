﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog.Models
{

        public class Comment
        {
            [Key]
            public int ID { set; get; }
            public String Title { set; get; }
            public String Body { set; get; }
        }
}